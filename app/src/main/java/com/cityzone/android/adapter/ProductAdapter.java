package com.cityzone.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cityzone.android.R;
import com.cityzone.android.models.BaseModel;
import com.cityzone.android.models.Product;
import com.cityzone.android.models.ProductModel;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by sang on 10/6/2015.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Product> listProducts = new ArrayList<Product>();


    public ProductAdapter(Context mContext, ArrayList<Product> listProducts) {
        this.mContext = mContext;
        this.listProducts = listProducts;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ProductAdapter.ViewHolder holder, int position) {
        Product product = listProducts.get(position);
        if(product.Image != null && !product.Image.isEmpty())
            Picasso.with(mContext).load(product.Image).into(holder.ivProduct);
        holder.tvNameProduct.setText(product.Name);
        holder.tvPriceProduct.setText(product.CurrentPrice + " VND");
    }

    @Override
    public int getItemCount() {
        return listProducts.size();
    }

    public void addAll(ArrayList<Product> listProductResult) {
        listProducts.addAll(listProductResult);
        this.notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProduct;
        TextView tvNameProduct;
        TextView tvPriceProduct;


        public ViewHolder(View itemView) {
            super(itemView);
            ivProduct = (ImageView) itemView.findViewById(R.id.ivProduct);
            tvNameProduct = (TextView) itemView.findViewById(R.id.tvNameProduct);
            tvPriceProduct = (TextView) itemView.findViewById(R.id.tvPriceProduct);
        }
    }
}
