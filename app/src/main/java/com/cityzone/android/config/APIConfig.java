package com.cityzone.android.config;

/**
 * Created by sang on 10/6/2015.
 */
public class APIConfig {
    private static final String API_ROOT = "http://api.cityzone.vn/";


    /**
     * Response:
     * "{
     *    ""success"": true,
     *    ""statusCode"": 200, //Mã response,
     *    ""message"":  ""Server đang bảo trì!"", //Nếu có lỗi
     *    ""data"": {
     *        ""items"": [
     *            {
     *                ""Id"": 1,
     *                ""Name"": ""Gạo tám Hải Hậu"",
     *                ""OldPrice"": ""25"",
     *                ""CurrentPrice"": ""18"",
     *    ""DiscountPercent"":  ""10"",
     *                ""Weight"": 0, //Chỉ tiêu để so sánh
     *                ""Image"": ""http://vgame.vinaphone.com.vn/group1/M00/00/03/wKjIbVLBEnGAbBcsAABRfpxMN1o842.png""
     *            },
     *             {
     *                ""Id"": 1,
     *                ""Name"": ""Gạo - Lạc vừng"",
     *                ""OldPrice"": ""100"",
     *                ""CurrentPrice"": ""200"",
     *    ""DiscountPercent"":  ""0"",
     *                ""Weight"": 0, //Chỉ tiêu để so sánh
     *                ""Image"": ""http://vgame.vinaphone.com.vn/group1/M00/00/03/wKjIbVLBEnGAbBcsAABRfpxMN1o842.png""
     *            },
     *        ],
     *        ""_meta"": {
     *            ""TotalCount"": 17, //Tổng số item
     *            ""PageCount"": 2, //Tổng số trang
     *            ""CurrentPage"": 1, //Trang h..."
     **/
    public static String getCategoryUrl() {
        return API_ROOT;
    }

    public static String getApiRoot() {
        return API_ROOT;
    }

}
