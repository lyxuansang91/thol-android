package com.cityzone.android.fragment;


import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cityzone.android.R;
import com.cityzone.android.adapter.ProductAdapter;
import com.cityzone.android.models.BaseModel;
import com.cityzone.android.models.Product;
import com.cityzone.android.models.ProductModel;
import com.cityzone.android.network.NetworkHelpers;

import java.util.ArrayList;
import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {


    private RecyclerView rvProductList;
    private ArrayList<Product> listProduct = new ArrayList<Product>();
    private ProductAdapter productAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static Fragment getNewInstance() {
        return new HomeFragment();
    }


    @Override
    public int getResourceId() {
        return R.layout.fragment_home;
    }


    @Override
    public void initView(View view) {
        rvProductList = (RecyclerView) view.findViewById(R.id.rvProductList);
//        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
//        gridLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvProductList.setHasFixedSize(true);
        rvProductList.setItemAnimator(new DefaultItemAnimator());
        rvProductList.setLayoutManager(linearLayoutManager);
        productAdapter = new ProductAdapter(mContext, listProduct);
        rvProductList.setAdapter(productAdapter);
    }

    @Override
    public void initData() {
        ArrayList<Product> listProductResult = new ArrayList<Product>();
        for (int i = 0; i < 10; i++) {
            Product product = new Product();
            product.id = i;
            product.Name = "Name " + i;
            product.OldPrice = i * 1000;
            product.CurrentPrice = product.OldPrice + (i * new Random().nextInt(10) + 1);
            product.Image = "http://vgame.vinaphone.com.vn/group1/M00/00/03/wKjIbVLBEnGAbBcsAABRfpxMN1o842.png";
            listProductResult.add(product);
        }
        productAdapter.addAll(listProductResult);
    }


}
