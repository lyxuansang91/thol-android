package com.cityzone.android.logcat;

import android.util.Log;


/**
 * Created by sang on 29/01/2015.
 */
public class CLog {

    private static final boolean DEBUG = true;

    public static int w(String tag, String msg) {
        return (DEBUG ? Log.w(tag, msg) : -1);

    }

    public static int d(String tag, String msg) {
        return (DEBUG ? Log.d(tag, msg) : -1);
    }

    public static int e(String tag, String msg) {
        return (DEBUG ? Log.e(tag, msg) : -1);
    }

    public static int v(String tag, String msg) {
        return (DEBUG ? Log.v(tag, msg) : -1);
    }

    public static int i(String tag, String msg) {
        return (DEBUG ? Log.i(tag, msg) : -1);
    }
}
