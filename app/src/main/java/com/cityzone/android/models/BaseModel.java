package com.cityzone.android.models;

/**
 * Created by sang on 10/6/2015.
 */
public class BaseModel<T> {
    public int statusCode;
    public String success;
    public String message;
    public int errorCode;
    public String errorName;
    public T data;
}
