package com.cityzone.android.models;

/**
 * Created by sang on 10/6/2015.
 */
public class Product {
    public int id;
    public String Name;
    public float OldPrice;
    public float CurrentPrice;
    public int DiscountPercent;
    public int Weight;
    public String Image;
}
