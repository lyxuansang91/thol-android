package com.cityzone.android.network;

/**
 * Created by sang on 03/02/2015.
 */
public class KeyValueObject {
    private String key;
    private Object value;

    public KeyValueObject(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public KeyValueObject() {
    }

    ;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
