package com.cityzone.android.network;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;

import com.cityzone.android.logcat.CLog;
import com.google.gson.Gson;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.DiskLruCache;
import com.squareup.okhttp.internal.Util;

import java.io.BufferedReader;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class NetworkHelpers {
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final String TAG = NetworkHelpers.class.getSimpleName();
    private static final int CACHE_SIZE = 10 * 1024 * 1024; // 10 MiB
    private static OkHttpClient _client;
    private static Cache _cache;

    public static void log(Response response) {
        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            CLog.i(TAG, responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }

        CLog.i(TAG, "Cache hit count:            "
                + _client.getCache().getHitCount());
        CLog.i(TAG, "Cache request count:        "
                + _client.getCache().getRequestCount());
        CLog.i(TAG, "Cache network count:        "
                + _client.getCache().getNetworkCount());
        CLog.i(TAG, "Cache size:                 "
                + _client.getCache().getSize());
        CLog.i(TAG, "Cache max size:             "
                + _client.getCache().getMaxSize());
        CLog.i(TAG, "Cache write abort:          "
                + _client.getCache().getWriteAbortCount());
        CLog.i(TAG, "Cache write success:        "
                + _client.getCache().getWriteSuccessCount());

    }

    public static void cancelRequestByTag(final Object tag) {
        new Runnable() {
            @Override
            public void run() {
                final OkHttpClient client = getOkHttpClient();
//                Log.d(TAG, "Cancelling request by tag: " + tag.toString());
                client.cancel(tag);
            }
        }.run();
    }

//    public static <T> void post(Context ctx, String url, ArrayList<NameValuePair> headerLists, String payload, final Class<T> type, final CUrlCallback<T> callback, final Activity callNetworkCallbackOnThisUi) {
//        CLog.d(TAG, "curl payload:" + payload);
//        post(ctx, url, headerLists, payload, type, callback, callNetworkCallbackOnThisUi, null);
//    }


    public static <T> void post(final Context ctx, final String url, String payload, final Class<T> type, final CUrlCallback<T> callback, final Activity callNetworkCallbackOnThisUi) {
//        CLog.d(TAG, "curl payload:" + payload);
        post(ctx, url, null, payload, type, callback, callNetworkCallbackOnThisUi, null);
    }

    public static <T> void post(final Context ctx, final String url, final HashMap<String, String> headerLists, String payload, final Class<T> type, final CUrlCallback<T> callback, final Activity callNetworkCallbackOnThisUi, Object tag) {

        String time = System.currentTimeMillis() + "";
        RequestBody requestBody = RequestBody.create(JSON, payload);
        Builder urlbuilder = new Builder().url(url);

        if (headerLists != null) {
            Iterator iter = headerLists.entrySet().iterator();
            while(iter.hasNext()) {
                Map.Entry mapEntry = (Map.Entry) iter.next();
                urlbuilder.addHeader(mapEntry.getKey().toString(), mapEntry.getValue().toString());
            }
//            for (ContentValues nameValuePair : headerLists)
//                urlbuilder.addHeader(nameValuePair., nameValuePair.getValue());
        }

        if (payload != null) {
            urlbuilder.addHeader("Accept", "application/json");
            urlbuilder.post(requestBody);
        }


        if (tag != null)
            urlbuilder.tag(tag);

        final Request request = urlbuilder.build();


        final String responseString = "";
        Cache cache = getCache(ctx);

        final String responseCache = getFromCache(ctx, url);
        CLog.d(TAG, "Cache response:" + responseCache);


        final OkHttpClient client = getOkHttpClient();

        if (cache != null) {
            client.setCache(cache);
        }

        if (url.startsWith("https://")) {
            client.setHostnameVerifier(new AllValidHostnameVerifier());

            try {
                client.setSslSocketFactory(new SSLUtil().getSSLContext()
                        .getSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

                e.printStackTrace();
                CLog.w(TAG, "Network failure: " + e.getMessage());
                if (type.isAssignableFrom(String.class)) {
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse((T) null, (T) responseCache);
//                        if (isSuccess) {
//                            deleteFromCache(ctx, url);
//                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse((T) null, (T) responseCache);
//                                if (isSuccess)
//                                    deleteFromCache(ctx, url);
                            }
                        });
                    }
                } else {
                    if (callNetworkCallbackOnThisUi == null) {
                        T dataCache = new Gson().fromJson(responseCache, type);
//                        T dataCache = JsonHelper.decode(responseCache, type);
                        boolean isSuccess = callback.onResponse((T) null, dataCache);
//                        if (isSuccess && dataCache != null) {
////                            deleteFromCache(ctx, url);
//                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                T dataCache = new Gson().fromJson(responseCache, type);
                                boolean isSuccess = callback.onResponse((T) null, dataCache);
//                                if (isSuccess && dataCache != null) {
//                                 //   deleteFromCache(ctx, url);
//                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                if (response.code() < 200 || response.code() >= 300) {
                    CLog.w(TAG,
                            "Network error with http code: " + response.code());
                }
                final String responseString = response.body().string();
                NetworkHelpers.log(response);
//                CLog.d(TAG, "response String:" + responseString);

                if (type.isAssignableFrom(String.class)) {
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse((T) responseString, (T) responseCache);
                        if (isSuccess && responseCache != null) {
                            deleteFromCache(ctx, url);
                        }

                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse((T) responseString, (T) responseCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                } else {
//					if(KTVApplication.DEV_MODE)Log.d(TAG, "Data type is not String: " + type.getSimpleName());
//					if(KTVApplication.DEV_MODE)Log.d(TAG, "Raw Data:" + responseString);
                    CLog.d(TAG, "raw data:" + responseString);
                    final T data = new Gson().fromJson(responseString, type);
                    final T dataCache =  new Gson().fromJson(responseCache, type);
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse(data, dataCache);
                        if (isSuccess && responseCache != null) {
//                            deleteFromCache(ctx, url);
                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse(data, dataCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                }
            }
        });


    }

    public static <T> void get(Context ctx, String url, final Class<T> type, final CUrlCallback<T> callback, Activity callNetworkCallbackOnThisUi) {
        get(ctx, url, null, type, callback, callNetworkCallbackOnThisUi, null);
    }

    public static <T> void get(Activity activity, String url, final Class<T> type, final CUrlCallback<T> callback) {
        get(activity, url, type, callback, activity);
    }


    public static boolean deleteFromCache(Context ctx, String url) {

        try {
            DiskLruCache diskCache = DiskLruCache.open(ctx.getCacheDir(), 201105, 2, CACHE_SIZE);
            diskCache.flush();
            String key = Util.hash(url);
            CLog.d(TAG, "cache key:" + key + ", cache url:" + url);
            return diskCache.remove(key);

        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public static String getFromCache(Context ctx, String url) {
        try {
            DiskLruCache diskCache = DiskLruCache.open(ctx.getCacheDir(), 201105, 2, CACHE_SIZE);
            diskCache.flush();
            String key = Util.hash(url);

            final DiskLruCache.Snapshot snapshot = diskCache.get(key);
            if (snapshot == null)
                return null;

            CLog.d(TAG, "cache key:" + key + ", cache url:" + url);
            String response = "";
            try {
                FilterInputStream bodyIn = new FilterInputStream(snapshot.getInputStream(1)) {
                    @Override
                    public void close() throws IOException {
                        snapshot.close();
                        super.close();
                    }
                };
                BufferedReader bf = new BufferedReader(new InputStreamReader(bodyIn));
                String line;
                while ((line = bf.readLine()) != null) {
                    response += line;
                }
                bodyIn.close();
            } catch (Exception e) {
                e.printStackTrace();
                response = null;
            }

            return response;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> void get(Context ctx, String url, HashMap<String, String> headerLists, final Class<T> type, final CUrlCallback<T> callback, final Activity callNetworkCallbackOnThisUi) {
        get(ctx, url, headerLists, type, callback, callNetworkCallbackOnThisUi, null);
    }

    /**
     * Response String or Object parse by GSON (if JSON format)
     *
     * @param ctx
     * @param type
     * @param callback
     * @param tag      - tag to cancel request
     */
    public static <T> void get(final Context ctx, final String url, final HashMap<String, String> headerLists, final Class<T> type, final CUrlCallback<T> callback, final Activity callNetworkCallbackOnThisUi, final Object tag) {

        String time = System.currentTimeMillis() + "";
        Builder urlbuilder = new Builder().url(url);

        if (headerLists != null) {
            Iterator iter = headerLists.entrySet().iterator();
            while(iter.hasNext()) {
                Map.Entry mapEntry = (Map.Entry) iter.next();
                urlbuilder.addHeader(mapEntry.getKey().toString(), mapEntry.getValue().toString());
            }
        }

        if (tag != null)
            urlbuilder.tag(tag);


        final Request request = urlbuilder
                .addHeader("time", time)
                .build();

        Cache cache = getCache(ctx);

        final OkHttpClient client = getOkHttpClient();

        if (cache != null) {
            client.setCache(cache);
        }

        final String responseCache = getFromCache(ctx, url);


        if (url.startsWith("https://")) {
            client.setHostnameVerifier(new AllValidHostnameVerifier());

            try {
                client.setSslSocketFactory(new SSLUtil().getSSLContext()
                        .getSocketFactory());
            } catch (KeyManagementException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }


        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

                e.printStackTrace();
                CLog.w(TAG, "Network failure: " + e.getMessage());
                if (type.isAssignableFrom(String.class)) {
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse((T) null, (T) responseCache);
                        if (isSuccess && responseCache != null) {
//                            deleteFromCache(ctx, url);
                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse((T) null, (T) responseCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                } else {
                    if (callNetworkCallbackOnThisUi == null) {
                        T dataCache = new Gson().fromJson(responseCache, type);
                        boolean isSuccess = callback.onResponse((T) null, dataCache);
                        if (isSuccess) {
//                            deleteFromCache(ctx, url);
                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                T dataCache = new Gson().fromJson(responseCache, type);
                                boolean isSuccess = callback.onResponse((T) null, dataCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onResponse(final Response response) throws IOException {
                if (response.code() < 200 || response.code() >= 300) {
                    CLog.w(TAG,
                            "Network error with http code: " + response.code());
                }

                final String responseString = response.body().string();
                CLog.d(TAG, "response String:" + responseString);

//                CLog.d(TAG, "(on response)");

                if (type.isAssignableFrom(String.class)) {
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse((T) responseString, (T) responseCache);
                        if (isSuccess && responseCache != null) {
//                            deleteFromCache(ctx, url);
                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse((T) responseString, (T) responseCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                } else {
                    CLog.d(TAG, "raw data:" + responseString);
                    final T data = new Gson().fromJson(responseString, type);
                    final T dataCache = new Gson().fromJson(responseCache, type);
                    if (callNetworkCallbackOnThisUi == null) {
                        boolean isSuccess = callback.onResponse(data, dataCache);
                        if (isSuccess && responseCache != null) {
//                            deleteFromCache(ctx, url);
                        }
                    } else {
                        callNetworkCallbackOnThisUi.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                boolean isSuccess = callback.onResponse(data, dataCache);
                                if (isSuccess && responseCache != null) {
//                                    deleteFromCache(ctx, url);
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    private static Cache getCache(Context ctx) {
        if (_cache == null) {
            try {
                _cache = new Cache(ctx.getCacheDir(), CACHE_SIZE);
            } catch (IOException e1) {
                e1.printStackTrace();
                CLog.w(TAG, "Cannot create cache: " + e1.getMessage());
                _cache = null;
            }
        }
        return _cache;
    }

    private static OkHttpClient getOkHttpClient() {
        if (_client == null) {
            _client = new OkHttpClient();
            _client.setConnectTimeout(5000, TimeUnit.MILLISECONDS);
            _client.setReadTimeout(5000, TimeUnit.MILLISECONDS);
        }

        return _client;
    }

    public interface CUrlCallback<T> {
        boolean onResponse(T result, T resultCache);
    }
}
